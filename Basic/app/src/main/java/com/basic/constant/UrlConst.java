package com.basic.constant;

/**
 * Created by ybk on 2015/9/13.
 */
public class UrlConst {

    public static final String SERVICE_URL = "http://phoneapi.goujiawang.com/";

    public static final String HOUSE_TYPE_AND_STYLE = "area/getHouseTypeAndStyle.html";
}
