package com.basic.utils;

import android.app.Activity;
import android.widget.Toast;

public class ToastUtils {
    public static void ToastMakeText(Activity activity, String message) {
        if (activity != null) {
            Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();
        }
    }
}
