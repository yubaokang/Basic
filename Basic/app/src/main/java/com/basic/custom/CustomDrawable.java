//package com.basic.custom;
//
//import android.content.Context;
//import android.content.res.Resources;
//import android.graphics.Bitmap;
//import android.graphics.BitmapShader;
//import android.graphics.Canvas;
//import android.graphics.Paint;
//import android.graphics.Shader;
//import android.util.AttributeSet;
//
//import com.baoyz.widget.PullRefreshLayout;
//import com.baoyz.widget.RefreshDrawable;
//
//import org.xmlpull.v1.XmlPullParser;
//import org.xmlpull.v1.XmlPullParserException;
//
//import java.io.IOException;
//
///**
// * Created by ybk on 2015/9/26.
// */
//public class CustomDrawable extends RefreshDrawable {
//
//    private boolean isRunning;
//    private Paint mPaint;
//    private int mWidth;
//    private Bitmap mBitmap;
//
//    @Override
//    public void inflate(Resources r, XmlPullParser parser, AttributeSet attrs) throws XmlPullParserException, IOException {
//        super.inflate(r, parser, attrs);
//    }
//
//    public CustomDrawable(Context context, PullRefreshLayout layout) {
//        super(context, layout);
//    }
//
//    public void setmBitmap(Bitmap mBitmap) {
//        this.mBitmap = mBitmap;
//        BitmapShader bitmapShader = new BitmapShader(mBitmap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);
//        mPaint = new Paint();
//        mPaint.setAntiAlias(true);
//        mPaint.setShader(bitmapShader);
//        mWidth = Math.min(mBitmap.getWidth(), mBitmap.getHeight());
//    }
//
//    @Override
//    public void setPercent(float percent) {
//        // Percentage of the maximum distance of the drop-down refresh.
//    }
//
//    @Override
//    public void setColorSchemeColors(int[] colorSchemeColors) {
//
//    }
//
//    @Override
//    public void offsetTopAndBottom(int offset) {
//        // Drop-down offset.
//    }
//
//    @Override
//    public void start() {
//        isRunning = true;
//        // Refresh started, start refresh animation.
//    }
//
//    @Override
//    public void stop() {
//        isRunning = false;
//        // Refresh completed, stop refresh animation.
//    }
//
//    @Override
//    public boolean isRunning() {
//        return isRunning;
//    }
//
//    @Override
//    public void draw(Canvas canvas) {
////        canvas.drawCircle(mWidth / 2, mWidth / 2, mWidth / 2, mPaint);
//        canvas.drawText("下拉刷新", 2 * mWidth, 2 * mWidth, mPaint);
//    }
//}