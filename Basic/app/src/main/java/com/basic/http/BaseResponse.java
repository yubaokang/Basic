package com.basic.http;

/**
 * Created by ybk on 2015/9/5.
 */
public class BaseResponse {
   private String msg;
   private int returnCode = 200;
   private int taskType;

   public String getMsg() {
      return msg;
   }

   public void setMsg(String msg) {
      this.msg = msg;
   }

   public int getReturnCode() {
      return returnCode;
   }

   public void setReturnCode(int returnCode) {
      this.returnCode = returnCode;
   }

   public int getTaskType() {
      return taskType;
   }

   public void setTaskType(int taskType) {
      this.taskType = taskType;
   }
}
