package com.basic.view.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import com.andview.refreshview.XRefreshView;
import com.basic.R;
import com.basic.model.eventbus.Test;
import com.basic.view.adapter.CommonAdapter;
import com.basic.view.adapter.ViewHolder;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;

import de.greenrobot.event.EventBus;

@EActivity(R.layout.activity_main)
public class MainActivity extends BaseActivity {
   @ViewById(R.id.custom_view)
   XRefreshView custom_view;

   @ViewById(R.id.listView)
   ListView listView;

   /*@ViewById(R.id.recyclerView)
   RecyclerView recyclerView;
   */
   //    @ViewById
   //    PullRefreshLayout swipeRefreshLayout;
   @Click(R.id.button)
   public void click() {
      Intent intent = new Intent(this, SecondActivity_.class);
      startActivity(intent);
   }

   @AfterViews
   void init() {
      //        CustomDrawable customDrawable = new CustomDrawable(this, swipeRefreshLayout);
      //        customDrawable.setmBitmap(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher));
      //        swipeRefreshLayout.setRefreshDrawable(customDrawable);
      //        swipeRefreshLayout.setRefreshStyle(4);
      // 设置是否可以下拉刷新
      custom_view.setPullRefreshEnable(true);
      // 设置是否可以上拉加载
      custom_view.setPullLoadEnable(true);
      // 设置上次刷新的时间
      //custom_view.restoreLastRefreshTime(2015);
      // 设置时候可以自动刷新
      //        custom_view.setAutoRefresh(true);
      custom_view.setPullLoadEnable(true);
      custom_view.setScrollDuring(100);

      custom_view.setXRefreshViewListener(new XRefreshView.SimpleXRefreshListener() {
         @Override
         public void onRefresh() {
            super.onRefresh();
         }

         @Override
         public void onLoadMore(boolean isSlience) {
            super.onLoadMore(isSlience);
         }
      });

      List<Object> list = new ArrayList<>();
      for (int i = 0; i < 1; i++) {
         list.add("https://ss0.baidu.com/-Po3dSag_xI4khGko9WTAnF6hhy/super/whfpf%3D425%2C260%2C50/sign=c5c59c487ad98d1076815f7147028c3c/f603918fa0ec08faba2519135fee3d6d55fbda59.jpg");
         list.add("https://ss3.baidu.com/9fo3dSag_xI4khGko9WTAnF6hhy/super/whfpf%3D425%2C260%2C50/sign=df21f04ca186c91708560179af0044fd/5882b2b7d0a20cf47b46ea6b70094b36adaf99ab.jpg");
         list.add("https://ss3.baidu.com/9fo3dSag_xI4khGko9WTAnF6hhy/super/whfpf%3D425%2C260%2C50/sign=dca1f7bdb0003af34def8f205317f26e/4bed2e738bd4b31c6d73830f81d6277f9e2ff833.jpg");
         list.add("https://ss0.baidu.com/94o3dSag_xI4khGko9WTAnF6hhy/super/whfpf%3D425%2C260%2C50/sign=d9a2ba688c13632715b89173f7b294de/0824ab18972bd407fd87f5497d899e510eb309c5.jpg");
         list.add("http://img3.cache.netease.com/photo/0001/2015-09-24/B49HGQN900AO0001.jpg");
         list.add("http://img3.cache.netease.com/photo/0001/2015-09-24/B49HGQJ000AO0001.jpg");
         list.add("http://img4.cache.netease.com/photo/0001/2015-09-24/B49HGQ4A00AO0001.jpg");
         list.add("http://img4.cache.netease.com/photo/0001/2015-09-24/B49HGPUU00AO0001.jpg");
         list.add("http://img4.cache.netease.com/photo/0001/2015-09-24/B49HGQUK00AO0001.jpg");
         list.add("http://img3.cache.netease.com/photo/0001/2015-09-24/B49HDGDE00AO0001.jpg");
         list.add("http://img3.cache.netease.com/photo/0001/2015-09-24/B49HDGDF00AO0001.jpg");
         list.add("http://img5.cache.netease.com/photo/0001/2015-09-24/900x600_B49HDGDG00AO0001.jpg");
         list.add("http://img5.cache.netease.com/photo/0001/2015-09-24/900x600_B49HDGDH00AO0001.jpg");
         list.add("http://img5.cache.netease.com/photo/0001/2015-09-24/900x600_B49HDGDI00AO0001.jpg");
         list.add("http://img3.cache.netease.com/photo/0001/2015-09-24/900x600_B49HDGDJ00AO0001.jpg");
         list.add("http://pic.lvmama.com/opi/sy01-sydc-qz.jpg");
         list.add("http://pic.lvmama.com/opi/sy02-xgdsn-bj.jpg");
         list.add("http://pic.lvmama.com/opi/sy03-syzqdc-sh.jpg");
         list.add("http://pic.lvmama.com/opi/sy04-ymxyn-hz.jpg");
         list.add("http://pic.lvmama.com/opi/sy05-zqgqhlj-gz.jpg");
         list.add("http://pic.lvmama.com/opi/sy06-fsrmmxg.jpg");
         list.add("http://s3.lvjs.com.cn/uploads/pc/place2/2015-08-19/61a6e737-2fd7-4444-848d-8e33338c4c06.jpg");
         list.add("http://s3.lvjs.com.cn//uploads/pc/place2/2014-10-31/1414756320602.jpg");
         list.add("http://s1.lvjs.com.cn/uploads/pc/place2/84295/1441852305655.jpg");
         list.add("http://s3.lvjs.com.cn/uploads/pc/place2/84149/1421985044065.jpg");
         list.add("http://s1.lvjs.com.cn/uploads/pc/place2/2015-09-23/2a90aa67-7cf1-41c7-b364-1a2becd293e5.jpg");
         list.add("http://img.dajia365.com/0986b24549b67980a64a1b6c07396379.jpg");
         list.add("http://img.dajia365.com/49ca4b814bfc4086efdaf81f9f75b061.png");
         list.add("http://img.dajia365.com/f75435d065ab2e3c1b83d4b0785cadf0.png");
         list.add("http://img.dajia365.com/537b9eacb4eb21759ab75d31d300dba7.png");
         list.add("http://img.dajia365.com/3215f43c738109f7e47ed83952ce1ca5.png");
         list.add("http://img.dajia365.com/716f112cdef2d27d0b80e50246e7b205.png");
         list.add("http://img.dajia365.com/2cba1c81257d1dd4915edac5638fdc6c.png");
         list.add("http://img.dajia365.com/e96a6dd3bcec8cd540b52024e726d6ba.png");
         list.add("http://www.goujiawang.com/goujia/images/newIndex/banner1.jpg");
         list.add("http://www.goujiawang.com/goujia/images/newIndex/banner4.jpg");
         list.add("http://www.goujiawang.com/goujia/images/newIndex/banner1.jpg");
         list.add("http://cdn.goujiawang.com/store/website/uploaded/143459/5c7d935d8be4c5e4289d489e.jpg");
         list.add("http://cdn.goujiawang.com/store/website/uploaded/143459/03cceeae782492abca148b70.jpg");
         list.add("http://cdn.goujiawang.com/store/website//uploaded/143755/586f1eff0e8292bdae749b86.jpg");
         list.add("http://cdn.goujiawang.com/store/website//uploaded/143755/6287a5e861e7063517adb48d.jpg");
         list.add("http://cdn.goujiawang.com/store/website//uploaded/143534/aab86328d8becd91b61a7d98.jpg");
      }

      listView.setAdapter(new CommonAdapter<Object>(this, list, R.layout.item_main) {
         @Override
         public void convert(ViewHolder holder, Object o) {
            holder.setText(R.id.textView, o.toString());
            holder.setImage(R.id.imageView, o.toString());
            holder.setOnClickListener(R.id.textView, new View.OnClickListener() {
               @Override
               public void onClick(View v) {
               }
            });
         }
      });

   }

   public void onEventMainThread(Test test) {
      if (test != null)
         Toast.makeText(this, test.getNum() + "", Toast.LENGTH_LONG).show();
   }

   @Override
   protected void onDestroy() {
      super.onDestroy();
      EventBus.getDefault().unregister(this);
   }
}
