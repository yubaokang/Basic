package com.basic.view.activity;

import android.view.View;
import android.widget.TextView;

import com.basic.R;
import com.basic.model.eventbus.Test;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import de.greenrobot.event.EventBus;


@EActivity(R.layout.activity_second)
public class SecondActivity extends BaseActivity {

    @ViewById
    TextView textView2;

    @Click({R.id.textView2})
    void onClick(View view) {
        switch (view.getId()) {
            case R.id.textView2:
                Test test = new Test();
                test.setNum(1);
                EventBus.getDefault().post(test);
                break;
        }
    }
}
